package cl.usach.tingeso.repositories;

import cl.usach.tingeso.models.Student;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface StudentRepository extends CrudRepository<Student, Long> {

}
