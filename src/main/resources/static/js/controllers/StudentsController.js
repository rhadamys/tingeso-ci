app.controller('StudentsController', ['$scope','$http', function($scope,$http) {
	
    $scope.students = [];

    $scope.loadStudents = function(){
        $http.get('/students').then(function(response){
            $scope.students = response.data;
        });
    }

    $scope.loadStudents();

}]);